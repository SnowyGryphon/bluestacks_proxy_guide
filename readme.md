# How to setup bluestacks to use proxy per bluestacks instance
The basic steps are very simple:

1: Root the emulator with [BSTweaker](https://bstweaker.tk)

2: Install [proxydroid APK](https://play.google.com/store/apps/details?id=org.proxydroid&hl=en_US&gl=US)

This will allow proxy using HTTP, HTTPS, HTTPS tunneling, SOCKS 4, SOCKS 5 proxy protocols

# Below is a step by step guide

## 1: Create new bluestacks instance
![1](./img/1.PNG)
![2](./img/2.PNG)

## Choosing nougat 32 seems important

Could not get bstweaker to work on nougat 64, maybe that's just me

![3](./img/3.PNG)

## Enough resources to run [OSRS Emu Bot](https://snowygryphon.com/products/osrs_emu_bot)
![4](./img/4.PNG)


## Download BS Tweaker
https://bstweaker.tk/

![5](./img/5.PNG)

## Extract the zip
![6](./img/6.PNG)

## Run it
![7](./img/7.PNG)

## Select your newly created bluestacks instance and root it
Make sure bluestacks is turned off, then you should be able to unlock

![8](./img/8.PNG)

## Make sure the rooting was successful
![9](./img/9.PNG)

## Install and update SuperSu binaries
![10](./img/10.PNG)
![11](./img/11.PNG)

## After running the update bluestacks will shut down
![12](./img/12.PNG)

## Start up Bluestacks again and SuperSU app should now appear
![13](./img/13.PNG)

## Install proxydroid from google play
![14](./img/14.PNG)

## Grant it all the permissions
![15](./img/15.PNG)

## Just the simple proxy setup
![16](./img/16.PNG)

# WARNING
BSTweaker makes use of it's own ADB version.
This will cause OSRS Emu Bot to malfunction.
Therefore, after rooting bluestacks and shutting down BSTweaker do one of the following:

1: If you got ADB installed run the command adb kill-server

2: Restart your computer

## Summary

There you go, now proxydroid is installed.
Now you can clone this bluestacks instance and have separate proxies for each :)